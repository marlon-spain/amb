<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

//spatie
use Spatie\Permission\Models\Permission;

class SeederTablaPermisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos= [
            //tabla roles
            'ver-rol',
            'crear-rol',
            'editar-rol',
            'borrar-rol',
            
            //blog
            'ver-blog',
            'crear-blog',
            'editar-blog',
            'borrar-blog',
            
            //user
            'ver-user',
            'crear-user',
            'editar-user',
            'borrar-user',

            //dispositivos
            'ver-dispositivo',
            'crear-dispositivo',
            'editar-dispositivo',
            'borrar-dispositivo',
            
            


        ];
        foreach($permisos as $permiso){
            Permission::create(['name'=>$permiso]);
        }
    }
}

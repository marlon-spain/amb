<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('activity_logs');
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
         //   $table->json('user_permissions')->nullable(); //en lartavel 8 no funciona la inserccion json 
            $table->string('route_path');
            $table->string('route_method');
            $table->string('route_alias');
        //    $table->json('request_headers');
            $table->string('user_agent');
            $table->string('ip_address');
            $table->boolean('is_proxy');

            //$table->enigma();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}

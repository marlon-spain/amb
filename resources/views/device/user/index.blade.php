@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
<h1>Listado de isiarios</h1>
@stop

@section('content')
<a href="session" class="btn btn-primary mb-3">Crear Registro</a>

<table id="users" class="table table-striped">
<thead>
 <tr>
     <th scope="col">id</th>
     <th scope="col">name</th>
     <th scope="col">email</th>
     <th scope="col">Two factor</th>
     <th scope="col">acciones</th>
     
 </tr>

</thead> 
<tbody>
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>
            @if(isset($user->two_factor_secret))
            <i class="fa fa-check" style="color:green" aria-hidden="true"></i>
        @else
        <i class="fa fa-times" style="color:red" aria-hidden="true"></i>
        @endif
            </td>
            <td>
                <a class="btn btn-info"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;
                <button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i>
             </button>
            </td>        
        </tr>
    @endforeach
</tbody>
</table>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    
$(document).ready(function() {
    $('#users').DataTable({
       responsive:true,
       autowith:false,
      // "lengthMenu":[[5,10,50, -1],[5,10,50, "All"]]
      "language": {
            "lengthMenu": "Mostrar  "  +
                          `<select class="custom-select-sm custom-select-sm form-control form-control-sm">
                          <option value=   '5'>  5</option>
                          <option value=  '10'> 10</option>
                          <option value=  '25'> 25</option>
                          <option value=  '50'> 50</option>
                          <option value= '100'>100</option>
                          <option value=  '-1'>All</option>
                          </select>` +
                          "  Registros por paginas",
            "zeroRecords": "Nada encontrado - disculpas",
            "info": "Mostrando la pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "( Filtrado de _MAX_ registros totales)",
            'search': 'Buscar:',
            'paginate':{
                'next':'siguiente',
                'previous':'anterior'
            }
        }
    });
} );
</script>

@stop


@extends('adminlte::page')

@section('title', 'sesiones')

@section('content_header')
<h1>Listado de sesiones</h1>
@stop

@section('content')

<table id="sessions" class="table table-striped">
<thead>
 <tr>
    <th scope="col">Agent</th>
     <th scope="col">IP</th>
     <th scope="col">Actividad</th>
     <th scope="col">acciones</th>
     
     
 </tr>

</thead> 
<tbody>
    @foreach($sessions as $session)
        <tr>
            <td>{{$session->user_agent }}</td>
            <td>{{$session->ip_address}}</td>
            <td>{{ \Carbon\Carbon::createFromTimeStamp($session->last_activity)->diffForhumans() }}</td>
            <td>
                
                <button type="button" name="button" class="btn btn-danger delete-session" data-id="{{ $session->id }}">Borrar session</button>
            </td>        
        </tr>
    @endforeach
</tbody>
</table>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
    $(".delete-session").click(function() {
        
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax({
            responsive:true,
            autowith:false,
            url: "/delete-session",
            type: 'POST',
            data: {
                "id": id,
                "_token": token,
            },
            success: function() {
                location.reload();
            }
        });
    });
</script>

<script>
    
$(document).ready(function() {
    $('#sessions').DataTable({
       responsive:true,
       autowith:false,
      // "lengthMenu":[[5,10,50, -1],[5,10,50, "All"]]
      "language": {
            "lengthMenu": "Mostrar  "  +
                          `<select class="custom-select-sm custom-select-sm form-control form-control-sm">
                          <option value=   '5'>  5</option>
                          <option value=  '10'> 10</option>
                          <option value=  '25'> 25</option>
                          <option value=  '50'> 50</option>
                          <option value= '100'>100</option>
                          <option value=  '-1'>All</option>
                          </select>` +
                          "  Registros por paginas",
            "zeroRecords": "Nada encontrado - disculpas",
            "info": "Mostrando la pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "( Filtrado de _MAX_ registros totales)",
            'search': 'Buscar:',
            'paginate':{
                'next':'siguiente',
                'previous':'anterior'
            }
        }
    });
} );
</script>
@stop


@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
<h1>Listado de Roles</h1>
@stop

@section('content')

@can('crear-rol')
<a href="{{route('roles.create')}}" class="btn btn-primary mb-3">Crear Roles</a>
@endcan


<table id="roles" class="table table-striped">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">name</th>        
            <th scope="col">acciones</th>

        </tr>
    </thead>

    <tbody>
        @foreach ($roles as $role)
        <tr>
        <td>{{$role->id}}</td>
          <td>{{$role->name}}</td>
          <td>
            @can('editar-rol')
             <a class="btn btn-primary"  href="{{ route('roles.edit', $role->id)}}"><i class="fa fa-edit" aria-hidden="true"></i></a>
            @endcan
            
             @can('borrar-rol')
             {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
             {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>',['class'=>'btn btn-danger']) !!}
             {!! Form::close() !!}
             @endcan
          </td>

        </tr>
        @endforeach


    </tbody>
</table>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#roles').DataTable({
            responsive: true,
            autowith: false,
            // "lengthMenu":[[5,10,50, -1],[5,10,50, "All"]]
            "language": {
                "lengthMenu": "Mostrar  " +
                    `<select class="custom-select-sm custom-select-sm form-control form-control-sm">
                          <option value=   '5'>  5</option>
                          <option value=  '10'> 10</option>
                          <option value=  '25'> 25</option>
                          <option value=  '50'> 50</option>
                          <option value= '100'>100</option>
                          <option value=  '-1'>All</option>
                          </select>` +
                    "  Registros por paginas",
                "zeroRecords": "Nada encontrado - disculpas",
                "info": "Mostrando la pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "( Filtrado de _MAX_ registros totales)",
                'search': 'Buscar:',
                'paginate': {
                    'next': 'siguiente',
                    'previous': 'anterior'
                }
            }
        });
    });
</script>

@stop
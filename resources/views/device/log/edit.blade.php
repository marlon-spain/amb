@extends('layouts.app')

@section('content')
<div class="container">
    
    
                    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Actualizacion de Moviles</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('periferico.update') }}" enctype="multipart/form-data">
                        @csrf
                         
                         <input type="hidden" name="periferico_id" value="{{$periferico->id}}" />                         
                        

                        <div class="form-group row">
                            <label for="periferico" class="col-md-4 col-form-label text-md-right">Periferico</label>

                            <div class="col-md-6">
                                <input id="periferico" type="number"  class="form-control{{ $errors->has('periferico') ? ' is-invalid' : '' }}" name="periferico" value="{{ $periferico->periferico }}" >

                                @if ($errors->has('periferico'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('periferico') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                   

                        <div class="form-group row">
                            <label for="modelo" class="col-md-4 col-form-label text-md-right">Modelo</label>

                            <div class="col-md-6">
                                <input id="modelo" type="text"   class="form-control{{ $errors->has('modelo') ? ' is-invalid' : '' }}" name="modelo" value="{{$periferico->modelo}}" >

                                @if ($errors->has('modelo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('modelo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
			<div class="form-group row">
                            <label for="serie" class="col-md-4 col-form-label text-md-right">Nº serie</label>

                            <div class="col-md-6">
                                <input id="serie" type="text"   class="form-control{{ $errors->has('serie') ? ' is-invalid' : '' }}" name="serie" value="{{$periferico->serie}}" >

                                @if ($errors->has('serie'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('serie') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>				
                        
                        <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">Descripcion</label>
                         <div class="col-md-7">
                             <textarea id="description" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' :'' }}"  >{{$periferico->description}}
                             </textarea>
                             
                             @if($errors->has('description'))
                             <span class="invalid-feedback" role="alert">
                                 <strong> {{ $errors->first('description') }} </strong> 
                             </span>
                             @endif
                             
                         </div>
                    </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Actualizar campos
                                </button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
             <a href="{{route('periferico.index')}}" >
                    <button type="button" class="btn btn-info">VOLVER ATRAS</button>
                    </a> 
        </div>
    </div>
</div>
@endsection

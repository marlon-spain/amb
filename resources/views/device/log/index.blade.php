@extends('adminlte::page')

@section('title', 'logs')

@section('content_header')
<h1>Comportamientos de logs</h1>
@stop

@section('content')
<a href="register" class="btn btn-primary mb-3">Crear Registro</a>

<table id="logs" class="table table-striped">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Route path</th>
            <th scope="col">Route Method</th>
            <th scope="col">Route Alias</th>
            <th scope="col">ip address</th>
            <th scope="col">Fecha creacion</th>
            

        </tr>

    </thead>

<!--se eliminA EL TBODY PORQUE VA VIA AJAX YA QUE LOS LOGS GENERARN MUCHOS REGISTROS Y TARDA EN LLEGAR LOS DATOS -->

</table>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#logs').DataTable({
            "ajax": "{{route('datatable.log')}}",
            "columns":[
                {data:'id'},
                {data:'route_path'},
                {data:'route_method'},
                {data:'route_alias'},
                {data:'ip_address'},
                {data:'created_at'},
            ],
            responsive: true,
            autowith: false,
            // "lengthMenu":[[5,10,50, -1],[5,10,50, "All"]]
            "language": {
                "lengthMenu": "Mostrar  " +
                    `<select class="custom-select-sm custom-select-sm form-control form-control-sm">
                          <option value=   '5'>  5</option>
                          <option value=  '10'> 10</option>
                          <option value=  '25'> 25</option>
                          <option value=  '50'> 50</option>
                          <option value= '100'>100</option>
                          <option value=  '-1'>All</option>
                          </select>` +
                    "  Registros por paginas",
                "zeroRecords": "Nada encontrado - disculpas",
                "info": "Mostrando la pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "( Filtrado de _MAX_ registros totales)",
                'search': 'Buscar:',
                
            }
        });
    });
</script>

@stop
@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-5">
          
                    <div class="card-header">Detalles del Periferico</div>
                    <hr>
                    Datos: 
                    {{Auth::user()->email}} || {{Auth::user()->name}} 
                    <hr>                        


                    <div class="card-header">
                        <h6>Fecha creacion: {{$periferico->created_at}}</h6>
                        <h6>Planta: {{$periferico->user_id}}</h6>  

                        <table class="table">
                            <div class="form-group row">

                                <tbody>                                   
                                   
                                    <tr class="table-light">
                                        <th scope="row">Periferico/Censo:</th>
                                        <td>{{$periferico->periferico}}</td> 
                                    </tr>
                                    <tr>
                                        <th scope="row">Modelo:</th>
                                        <td>{{$periferico->modelo}}</td>                            
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Nª serie</th>
                                        <td>{{$periferico->serie}} </td>                                    
                                    </tr>
                                    <tr>
                                        <th scope="row">Descripcion:</th>
                                        <td>{{$periferico->description}} </td>                                    
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Creacion:</th>
                                        <td>{{$periferico->created_at}}</td>                                    
                                    </tr>
                                    <tr>
                                        <th scope="row">Modificacion:</th>
                                        <td>{{$periferico->updated_at}}</td>                                    
                                    </tr>                            

                                      <tr class="table-light">
                                        <th scope="row">DESCRIPCION:</th><br>
                                     <td>{{$periferico->description}}</td>                                         
                                </tr>

                                <tr>
                                    <th scope="row">
                                        <!-- Button to Open the Modal -->
                                       <a href="{{route('periferico.edit', ['id'=>$periferico->id])}}" class="btn btn-primary">Modificar</a>
                                    </th>    
									 <th>
									 <!-- Button to Open the Modal -->
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModall">
                                        Eliminar
                                    </button>
                                    <!-- The Modal -->
                                    <div class="modal" id="myModall">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h4 class="modal-title">¿Estas seguro?</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                    Si eliminas este registro nunca podras recuperarlo. ¿Estas seguro de querer eliminarla?
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                                                    <a href="{{route('periferico.delete', ['id'=>$periferico->id])}}" class="btn btn-danger">Borrar definitivamente</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>																  
                                        </th>
                                    </td>									
                                </tr>
								
								
                                </tbody>
                        </table>
                    </div>                                  
                    <a href="{{route('periferico.index')}}" >
                    <button type="button" class="btn btn-info">VOLVER ATRAS</button>
                    </a> 

                </div>        
            </div>
     
        @endsection

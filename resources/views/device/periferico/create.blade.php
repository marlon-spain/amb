@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registro de Periferico</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('periferico.save') }}" aria-label="{{ __('Register') }}">
                        @csrf                       

                        <div class="form-group row">
                            <label for="periferico" class="col-md-4 col-form-label text-md-right">Periferico</label>

                            <div class="col-md-6">
                                <input id="periferico" type="number" placeholder="numero de censo"  class="form-control{{ $errors->has('periferico') ? ' is-invalid' : '' }}" name="periferico" value="{{ old('periferico') }}" required>

                                @if ($errors->has('periferico'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('periferico') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                       

                        <div class="form-group row">
                            <label for="modelo" class="col-md-4 col-form-label text-md-right">Modelo</label>

                            <div class="col-md-6">
                                <input id="modelo" type="text" placeholder="modelo"  class="form-control{{ $errors->has('modelo') ? ' is-invalid' : '' }}" name="modelo" required>

                                @if ($errors->has('modelo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('modelo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
			<div class="form-group row">
                            <label for="serie" class="col-md-4 col-form-label text-md-right">Nªserie o IMEI</label>

                            <div class="col-md-6">
                                <input id="serie" type="text" placeholder="Nº de serie o IMEI"  class="form-control{{ $errors->has('serie') ? ' is-invalid' : '' }}" name="serie" >

                                @if ($errors->has('serie'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('serie') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>		
                        
                        <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">Descripcion</label>
                        <div class="col-md-7">
                            <textarea id="description" name="description" placeholder="si es necesario"  class="form-control"></textarea>                            
                            @if($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif                            
                        </div>
                    </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                                <input type="reset" class="btn btn-danger" value="Reset"/> 
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
            <a href="{{route('periferico.index')}}" >
                    <button type="button" class="btn btn-info">VOLVER ATRAS</button>
                    </a> 
        </div>
    </div>
</div>
@endsection
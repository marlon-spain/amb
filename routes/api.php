<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ActivityLogController;
use App\Http\Controllers\PlantaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('RegisterLog')->group(function () { //Registro de actividad Logs

Route::group(['prefix' => 'planta'], function () {
    $controller = PlantaController::class;
    Route::get('/',           [$controller, 'index']); 

});

});

//esto es nuevo 
//zAbTxsY7e8JurVwctdfV
<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ActivityLogController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('RegisterLog')->group(function () { //Registro de actividad Logs

Route::get('/', function () {
    return view('auth/login');
});

//sesiones
Route::middleware('auth')->group(function() {
    Route::get('sessions', 'App\Http\Controllers\UserController@session')->name('session');
    Route::post('delete-session', 'App\Http\Controllers\UserController@delete');      
});

//spatie
Route::group(['middleware' =>['auth']], function(){
    Route::resource('users', UserController::class);
    Route::resource('roles', RolController::class);
});



Route::middleware(['auth:sanctum', 'verified'])->get('/dash', function () {
    return view('dash.index');
})->name('dash');



//lgoa
Route::resource('logs', 'App\Http\Controllers\ActivityLogController');
Route::get('datatable/logs', 'App\Http\Controllers\ActivityLogController@ajaxAllLogs')
->name('datatable.log'); //https://www.youtube.com/watch?v=Hsw4GGeolmg


}); //cierre log





//plantas



//perifericos
//rutas para almacen B2 periferico




//moviles
//equipos
//imppresoras
//comnent nuevo
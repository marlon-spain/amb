<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ActivityLog as model;
use App\Models\ActivityLog;
use Illuminate\Support\Facades\Log;

class ActivityLogController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-rol | crear-rol | editar-rol| borrar-rol', ['only'=>['index']]);
        $this->middleware('permission:crear-rol',  ['only'=>['create','store']]);
        $this->middleware('permission:editar-rol', ['only'=>['edit','update']]);
        $this->middleware('permission:borrar-rol', ['only'=>['destroy']]);
    } 

    public function index()
    {
        $logs = ActivityLog::select('*')->orderBy('id', 'desc')->get();
        return view('device.log.index')->with('logs',$logs);  

    }

    public function ajaxAllLogs()
    {
      $logs = ActivityLog::select('id','route_path','route_method','route_alias','ip_address','created_at')->get();
       //utilizar paquete
      //https://packagist.org/packages/yajra/laravel-datatables-oracle
      return datatables()->of($logs)->toJson();
    }

    public function search(Request $request)
    {
        $model = new model();
        $query = $model->searchWhere($request);
        return 
        $this->successfulResponse($query);
    }

    public function storeActivity(Request $request)
    {
        $authUser = $request->user();

        $activity = [
            "user_id" => $authUser->id ?? null,
            "user_permissions" => $authUser != null && $authUser->permissions !== null ? $authUser->permissions->toArray() : null,
            "route_path"=>$request->path() . ($request->getBaseUrl() . $request->getPathInfo() == '/' ? '/?' : '?') . $request->getQueryString(),
            "route_method" => $request->getMethod(),
            "route_alias" => (empty($request->route()->getName())) ? $request->route()->getPrefix() : $request->route()->getName(),
            "request_headers" => json_encode($request->header()),
            "user_agent" => $request->header('User-Agent') ?? 'Not defined',
            "ip_address" => $request->ip(),
            "is_proxy" => $request->isFromTrustedProxy()
        ];
        $result = model::storeActivity($activity);
        return $result;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planta extends Model
{
      protected $table = 'plantas';
      
    public function equipos(){
        return $this->hasMany('App\Equipo');
    }
	
	public function moviles(){
        return $this->hasMany('App\Movil');
    }
    
    
    
}

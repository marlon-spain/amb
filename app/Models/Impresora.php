<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Impresora extends Model
{
     protected $table = 'impresoras';
     //relacion one to many: una planta tendra muchos equipos
    public function user(){       
         return $this->belongsTo('APP\User', 'user_id');
    }
    public function planta(){       
         return $this->belongsTo('APP\Planta', 'planta_id');
    }
}

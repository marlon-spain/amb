<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Periferico extends Model
{
     protected $table = 'perifericos';
     //relacion one to many: una planta tendra muchos equipos
    public function user(){       
         return $this->belongsTo('APP\User', 'user_id');
    }
  
}
